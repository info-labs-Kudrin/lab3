﻿#include "pch.h"
#include "CppUnitTest.h"
#include "../LAB3/HashGenerator.h"
#include "../LAB3/redBlackNode.h"
#include "../LAB3/redBlackTree.h"
#include "../LAB3/Complex.h"
#include <fstream>
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(RBtreeTESTS)
	{
	public:
		
		TEST_METHOD(A_NodeTests)
		{
			Assert::IsFalse(getHash(1) == getHash(2));
			Assert::IsFalse(getHash(1.0) == getHash(2.0));
			Assert::IsFalse(getHash(0) == getHash(1));
			Assert::IsFalse(getHash(0) != getHash(0));
			Assert::IsFalse(getHash(0.0) != getHash(0.0));
			Assert::IsFalse(getHash(Complex(1)) == getHash(Complex(2)));
			Assert::IsFalse(getHash(Complex(0, 1.0)) == getHash(Complex(0, 2.0)));
			Assert::IsFalse(getHash(Complex(123,321)) == getHash(Complex(15, -123)));
			Assert::IsFalse(getHash(Complex(0)) != getHash(Complex(0)));
			Assert::IsFalse(getHash(Complex(876, 140)) != getHash(Complex(876, 140)));

			RB<int> n1, n2(3);
			RB<double> n3, n4(3.0);
			RB<Complex> n5, n6(Complex(3.0, 4));
			Assert::IsTrue(*(n1.getInfo()) == 0 && *(n2.getInfo()) == 3);
			Assert::IsTrue(*(n3.getInfo()) == 0.0 && *(n4.getInfo()) == 3.0);
			Assert::IsTrue(*(n5.getInfo()) == Complex(0, 0) && *(n6.getInfo()) == Complex(3.0, 4));
			Assert::IsTrue(n6.getKey() == getHash(Complex(3.0, 4)));
			Assert::IsTrue(n5.getKey() == getHash(Complex(0, 0)));
			Assert::IsTrue(n1.getColor() == 0);
			n1.setColor(n1.getColor() + 1);
			Assert::IsTrue(n1.getColor() == 1);
			Assert::ExpectException<int>([&n1]() {n1.setColor(3); });
			Assert::ExpectException<int>([&n1]() {n1.setColor(-1); });
			Assert::ExpectException<int>([&n2]() {n2.setColor(3); });
			Assert::ExpectException<int>([&n2]() {n2.setColor(-1); });
			n1.setColor(2);

			RB<Person> npers(Person(0, "Max", 2));
		}
		TEST_METHOD(B_PushFindTests)
		{
			string way;
			redBlackTree<int> a;
			Assert::IsTrue(!a.find(0, way) && way == "");
			a.push(5);
			Assert::IsTrue(a.find(5, way) && way == "");
			a.push(6);
			Assert::IsTrue(a.find(6, way) && way == "r");

			RB<int> *x = a.getNodeCopy(5), *y;
			Assert::IsTrue(x->getColor() == 1 && x->left == 0 && x->right == 0 && *(x->getInfo()) == 5);
			y = a.getNodeCopy(6);
			Assert::IsTrue(y->getColor() == 0 && y->left == 0 && y->right == 0 &&  *(y->getInfo()) == 6);
			a.push(7);
			Assert::IsTrue(a.find(7, way) && way == "r" && a.find(6, way) && way == "" && a.find(5, way) && way == "l");
			delete x, y;
			x = a.getNodeCopy(5);
			Assert::IsTrue(x->getColor() == 0);
			delete x;
			x = a.getNodeCopy(6);
			Assert::IsTrue(x->getColor() == 1);
			delete x;
			x = a.getNodeCopy(7);
			Assert::IsTrue(x->getColor() == 0);
			delete x;
			a.push(8);
			x = a.getNodeCopy(8);
			Assert::IsTrue(a.find(8, way) && way == "rr" && x->getColor() == 0);
			delete x;
			x = a.getNodeCopy(5);
			Assert::IsTrue(a.find(5, way) && way == "l" && x->getColor() == 1);
			delete x;
			x = a.getNodeCopy(6);
			Assert::IsTrue(a.find(6, way) && way == "" && x->getColor() == 1);
			delete x;
			x = a.getNodeCopy(7);
			Assert::IsTrue(a.find(7, way) && way == "r" && x->getColor() == 1);
			delete x;
		}
		TEST_METHOD(C_TraversalsTests)
		{
			{
				redBlackTree<int> a;
				a.push(5);
				a.push(6);
				a.push(7);
				a.push(8);
				int sum = 0;
				Traversal::traversalReduce<int, int>("102", [](int& start, RB<int>* nd) {start += *(nd->getInfo()); }, sum, a);
				Assert::IsTrue(sum == 26);
				sum = 1;
				Traversal::traversalReduce<int, int>("102", [](int& start, RB<int>* nd) {start *= *(nd->getInfo()); }, sum, a);
				Assert::IsTrue(sum == 1680);
				Assert::ExpectException<int>([&sum, &a]() {Traversal::traversalReduce<int, int>("103", [](int& start, RB<int>* nd) {start *= *(nd->getInfo()); }, sum, a); });
				Assert::ExpectException<int>([&sum, &a]() {Traversal::traversalReduce<int, int>("111", [](int& start, RB<int>* nd) {start *= *(nd->getInfo()); }, sum, a); });
			}

			redBlackTree<Complex> a;
			a.push(Complex(1, 2));
			a.push(Complex(0, 1));
			a.push(Complex(1, 3));
			a.push(Complex(3, 4));
			a.push(Complex(5, -1));
			ofstream f;
			f.open("text.txt");
			f.close();
			a.traversalMap("102", [](RB<Complex>* nd) {
				ofstream f;
				f.open("text.txt", std::fstream::app);
				f << (*(nd->getInfo())).getReal() << " + " << (*(nd->getInfo())).getImag() << "i " << ((nd->getColor()) ? "BLACK " : "RED   ") << nd->getKey() << endl;
				f.close(); 
				});
			ifstream r;
			r.open("text.txt");
			string ans = "", def = "\n1 + 2i BLACK 8\n0 + 1i BLACK 2\n1 + 3i BLACK 13\n5 + -1i RED   9\n3 + 4i RED   32\n";
			while (!r.eof())
			{
				string s;
				getline(r, s);
				ans = ans + "\n" + s;
			}
			Assert::IsTrue(ans == def);
			Assert::ExpectException<int>([&a]() {a.traversalMap("103", [](RB<Complex>* a) {}); });
			Assert::ExpectException<int>([&a]() {a.traversalMap("002", [](RB<Complex>* a) {}); });
		}
		TEST_METHOD(D_EqualityTests)
		{
			redBlackTree<int> a, b, c, d;
			Assert::IsTrue(a == b);
			a.push(1); 
			Assert::IsFalse(a == b);
			b.push(1);
			Assert::IsTrue(a == b);
			a.push(2); b.push(2);
			Assert::IsTrue(a == b);
			a.push(3); b.push(3);
			Assert::IsTrue(a == b);
			a.push(4);
			Assert::IsFalse(a == b);
			c.push(1); d.push(0);
			Assert::IsFalse(c == d);
			c.push(0); d.push(1);
			Assert::IsFalse(c == d);
		}
		TEST_METHOD(E_SubTreesTests)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 7; i++)
				a.push(i);
			redBlackTree<int>* b = a.getSubTree(1);
			Assert::IsTrue(a == *b);
			Assert::ExpectException<int>([&a]() {a.getSubTree(-1); });
		}
		TEST_METHOD(F_ThreadingTest)
		{
			redBlackTree<Complex> a, *b;
			a.push(Complex(1, 2));
			a.push(Complex(0, 1));
			a.push(Complex(1, 3));
			a.push(Complex(3, 4));
			a.push(Complex(5, -1));
			ofstream f;
			f.open("text.txt");
			f.close();
			a.threadedTraversalMap("102", [](RB<Complex>* nd) {
				ofstream f;
				f.open("text.txt", std::fstream::app);
				f << (*(nd->getInfo())).getReal() << " + " << (*(nd->getInfo())).getImag() << "i " << ((nd->getColor()) ? "BLACK " : "RED   ") << nd->getKey() << endl;
				f.close(); 
				});
			ifstream r;
			r.open("text.txt");
			string ans = "", def = "\n1 + 2i BLACK 8\n0 + 1i BLACK 2\n1 + 3i BLACK 13\n5 + -1i RED   9\n3 + 4i RED   32\n";
			while (!r.eof())
			{
				string s;
				getline(r, s);
				ans = ans + "\n" + s;
			}
			r.close();
			Assert::IsTrue(ans == def);

			b = a.getSubTree(Complex(1, 2));
			f.open("text.txt");
			f.close();
			b->threadedTraversalMap("021", [](RB<Complex>* nd) {
				ofstream f;
				f.open("text.txt", std::fstream::app);
				f << (*(nd->getInfo())).getReal() << " + " << (*(nd->getInfo())).getImag() << "i " << ((nd->getColor()) ? "BLACK " : "RED   ") << nd->getKey() << endl;
				f.close();
				});
			r.open("text.txt");
			ans = "", def = "\n0 + 1i BLACK 2\n5 + -1i RED   9\n3 + 4i RED   32\n1 + 3i BLACK 13\n1 + 2i BLACK 8\n";
			while (!r.eof())
			{
				string s;
				getline(r, s);
				ans = ans + "\n" + s;
			}
			r.close();
			Assert::IsTrue(ans == def);
			Assert::ExpectException<int>([b]() {b->threadedTraversalMap("122", [](RB<Complex>* a) {}); });
			Assert::ExpectException<int>([b]() {b->threadedTraversalMap("123", [](RB<Complex>* a) {}); });
		}
		TEST_METHOD(G_FindSubTreeTest)
		{
			redBlackTree<int> a, b;
			string way;
			a.push(5);
			a.push(6);
			a.push(7);
			a.push(8);
			a.push(9);
			b.push(8);
			b.push(7);
			b.push(9);
			Assert::IsTrue(a.findSubTree(b, way) && way == "r");
			b.push(10);
			Assert::IsFalse(a.findSubTree(b, way));

		}
		TEST_METHOD(F_asTests)
		{
			size_t n = 6;
			Complex* arr = new Complex[n];
			for (size_t i = 0; i < n; i++)
				arr[i] = Complex(i, i);
			redBlackTree<Complex> tree(arr, n);
			vector<Complex> vect = tree.asVector("012");
			for (size_t i = 0; i < n; i++)
				Assert::IsTrue(vect[i] == arr[i]);
			Assert::IsTrue(tree.asString("021") == "0.000000 0.000000i 2.000000 2.000000i 5.000000 5.000000i 4.000000 4.000000i 3.000000 3.000000i 1.000000 1.000000i ");
		}
	};
	TEST_CLASS(SpeedTESTS)
	{
	public:
		TEST_METHOD(A_CreationDecomposition30000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 30000; i++)
				a.push(i);
		}
		TEST_METHOD(A_CreationDecomposition2000000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 2000000; i++)
				a.push(i);
		}
		TEST_METHOD(B_CTraversalD30000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 30000; i++)
				a.push(i);
			a.traversalMap("012", [](RB<int>* nd) {});
		}
		TEST_METHOD(B_CTraversalD2000000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 2000000; i++)
				a.push(i);
			a.traversalMap("012", [](RB<int>* nd) {});
		}
		TEST_METHOD(C_CwrongFindD30000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 30000; i++)
				a.push(i);
			string way;
			a.find(-1, way);
		}
		TEST_METHOD(C_CwrongFindD2000000)
		{
			redBlackTree<int> a;
			for (int i = 0; i < 2000000; i++)
				a.push(i);
			string way;
			a.find(-1, way);
		}
		TEST_METHOD(D_CwrongFindSubTreeD30000)
		{
			redBlackTree<int> a, b;
			b.push(-1);
			for (int i = 0; i < 30000; i++)
				a.push(i);
			string way;
			a.findSubTree(b, way);
		}
		TEST_METHOD(D_CwrongFindSubTreeD2000000)
		{
			redBlackTree<int> a, b;
			b.push(-1);
			for (int i = 0; i < 2000000; i++)
				a.push(i);
			string way;
			a.findSubTree(b, way);
		}
	};
}
