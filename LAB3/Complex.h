#pragma once
#include <iostream>
#include <cmath>

using namespace std;

class Complex
{
private:
	double x, y;
public:
	Complex();
	Complex(double x, double y);
	Complex(Complex x, Complex y);
	Complex(double x);

	double Module();

	operator double();
	Complex operator !();
	Complex operator +(const Complex& added);
	Complex operator -();
	Complex operator -(const Complex& added);
	Complex operator *(const Complex& b);
	Complex operator /(const double& b);
	Complex operator /(const Complex& b);
	Complex operator =(const int& b);
	Complex operator =(const double& b);
	bool operator ==(const Complex& b);
	bool operator !=(const Complex& b);

	void prt();
	double getReal();
	double getImag();

	friend std::ostream& operator<< (std::ostream& out, const Complex& point);
};