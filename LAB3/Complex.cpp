#include "Complex.h"

Complex::Complex() : Complex(0,0)
{

}

Complex::Complex(double x, double y)
{
	this->x = x;
	this->y = y;
}

Complex::Complex(Complex x, Complex y)
{
	this->x = x.x + y.x;
	this->y = x.y + y.y;
}

Complex::Complex(double x)
{
	this->x = x;
	this->y = 0;
}

double Complex::Module()
{
	return sqrt(this->x * this->x + this->y * this->y);
}

Complex::operator double()
{
	return this->x;
}

Complex Complex::operator!()
{
	return Complex(this->x, -this->y);
}

Complex Complex::operator+(const Complex &added)
{
	return Complex(this->x + added.x, this->y + added.y);
}

Complex Complex::operator-()
{
	return Complex(-this->x, -this->y);
}

Complex Complex::operator-(const Complex &added)
{
	Complex c = added;
	return Complex(*this) + -c;
}

Complex Complex::operator*(const Complex &b)
{
	return Complex(this->x * b.x - this->y * b.y, this->x * b.y + this->y * b.x);
}

Complex Complex::operator/(const double &b)
{
	return Complex(this->x / b, this->y / b);
}

Complex Complex::operator/(const Complex &b)
{
	Complex c = b;
	return (*this * !c / (double(c * (!c))));
}

Complex Complex::operator=(const int &b)
{
	this->x = b;
	this->y = 0;
	return *this;
}

Complex Complex::operator=(const double &b)
{
	this->x = b;
	this->y = 0;
	return *this;
}

bool Complex::operator==(const Complex& b)
{
	if (b.x == this->x && b.y == this->y)
		return true;
	return false;
}

bool Complex::operator!=(const Complex& b)
{
	if (*this == b)
		return false;
	return true;
}

void Complex::prt()
{
	cout << this->x << ((y < 0) ? "" : "+") << this->y << "*i";
}

double Complex::getReal()
{
	return this->x;
}

double Complex::getImag()
{
	return this->y;
}

std::ostream& operator<<(std::ostream& out, const Complex& point)
{
	return out << point.x << ((point.y < 0) ? "" : "+") << point.y << "*i";
}
