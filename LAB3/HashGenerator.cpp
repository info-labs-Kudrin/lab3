#include "HashGenerator.h"

long long getHash(int value) {
	return (long long)(value % 123456791);
}

long long getHash(double value) {
	return (long long)(value) % 123456791;
}

long long getHash(Complex value) {
	long long x = getHash(value.getReal()), y = getHash(value.getImag());
	return (long long)((x + y) * (x + y + 1) / 2 + y);
}

long long getHash(Person value){
	return value.getID();
}