#pragma once
#include <iostream>
#include <string>
using namespace std;

class Person {
private:
	long long schoolID;
	string firstName;
	string middleName;
	string lastName;
	long long birthDate;
public:
	Person(long long schoolID, string firstName, string middleName, string lastName, long long birthDate)
	{
		this->schoolID = schoolID;
		this->firstName = firstName;
		this->middleName = middleName;
		this->lastName = lastName;
		this->birthDate = birthDate;
	};
	Person(long long schoolID, string Name, long long birthDate) : Person(schoolID, Name, "no", "no", birthDate) {};
	Person(long long schoolID) : Person(schoolID, "-", "", "", 0) {};
	Person() : Person(0, "-", "", "", 0) {};
	long long getID() { return this->schoolID; }
	string getFirstName() { return this->firstName; };
	string getMiddleName() { return this->middleName; };
	string getLastName() { return this->lastName; };
	long long getBirthDate() { return this->birthDate; };
	bool operator==(const Person& other) { return (this->schoolID == other.schoolID); };
	bool operator!=(const Person& other) { return !(this->operator==(other)); };
	void prt(){	printf("%10lld. birthParameter: %10lld; name: ", schoolID, birthDate);	std::cout << firstName << " " << middleName << " " << lastName << endl;	};
};