#pragma once
#include <iostream>
#include <string>
#include "redBlackTree.h"
#include "Complex.h"
#include "School.h"
using namespace std;

#define RBT redBlackTree
#define forn(i, j, k) for (int i = j; i < k; i++)

class Console {
private:
	vector<RBT<int>> vI;
	vector<RBT<double>> vD;
	vector<RBT<Complex>> vC;
	vector<RBT<Person>> vP;

	static void crtIntTree(vector<RBT<int>>& v, size_t sz);
	static void crtDouTree(vector<RBT<double>>& v, size_t sz);
	static void crtComTree(vector<RBT<Complex>>& v, size_t sz);
	static void crtPerTree(vector<RBT<Person>>& v, size_t sz);

	static void pushInTree(vector<RBT<int>>& v, size_t index);
	static void pushDoTree(vector<RBT<double>>& v, size_t index);
	static void pushCoTree(vector<RBT<Complex>>& v, size_t index);
	static void pushPeTree(vector<RBT<Person>>& v, size_t index);

	static void findANode_int(vector<RBT<int>> v, size_t index);
	static void findANode_dou(vector<RBT<double>> v, size_t index);
	static void findANode_com(vector<RBT<Complex>> v, size_t index);
	static void findANode_per(vector<RBT<Person>> v, size_t index);

	static void rmInTree(vector<RBT<int>>& v, size_t index);
	static void rmDoTree(vector<RBT<double>>& v, size_t index);
	static void rmCoTree(vector<RBT<Complex>>& v, size_t index);
	static void rmPeTree(vector<RBT<Person>>& v, size_t index);

	static void tryAgain();
	static void standardAbracadabra();

	void dltTree();
	void getAsString();
	void howManyTrees();
	void seeIfEqual();
	void rmNode();
	void findNode();
	void pushInTree();
	void crtATree();
	void mainMenu();
public:
	Console();
	void console();
};