#include "Console.h"

Console::Console() {
	
}

void Console::tryAgain() {
	cout << "........................\n";
	cout << "Why don't you try again?\n";
	cout << "........................\n";
}

void Console::crtIntTree(vector<RBT<int>>& v, size_t sz)
{
	v.push_back(RBT<int>());
	cout << "........................\n";
	cout << "Please, type the values of the nodes\n";
	forn(i, 0, sz)
	{
		int val;
		cin >> val;
		v[v.size() - 1].push(val);
	}
	cout << "Success\n";
	cout << "........................\n";
}
void Console::crtDouTree(vector<RBT<double>>& v, size_t sz)
{
	v.push_back(RBT<double>());
	cout << "........................\n";
	cout << "Please, type the values of the nodes\n";
	forn(i, 0, sz)
	{
		double val;
		cin >> val;
		v[v.size() - 1].push(val);
	}
	cout << "Success\n";
	cout << "........................\n";
}
void Console::crtComTree(vector<RBT<Complex>>& v, size_t sz)
{
	v.push_back(RBT<Complex>());
	cout << "........................\n";
	cout << "Please, type the values of the nodes\n";
	forn(i, 0, sz)
	{
		double re, im;
		cin >> re >> im;
		v[v.size() - 1].push(Complex(re, im));
	}
	cout << "Success\n";
	cout << "........................\n";
}
void Console::crtPerTree(vector<RBT<Person>>& v, size_t sz)
{
	v.push_back(RBT<Person>());
	cout << "........................\n";
	cout << "Please, type the values of the nodes\n";
	cout << "Type first name, second name, last name, ID, birth date expression (must be integer)\n";
	cout << "Example: \"Joly-Maria Statsberg von-Grotte 10049 943330\"\n";
	forn(i, 0, sz)
	{
		string firstName, secondName, lastName;
		long long schoolID, birthDate;
		cin >> firstName >> secondName >> lastName >> schoolID >> birthDate;
		Person val(schoolID, firstName, secondName, lastName, birthDate);
		v[v.size() - 1].push(val);
	}
	cout << "Success\n";
	cout << "........................\n";
}


void Console::pushInTree(vector<RBT<int>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	int val;
	cin >> val;
	v[index].push(val);
}
void Console::pushDoTree(vector<RBT<double>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	double val;
	cin >> val;
	v[index].push(val);
}
void Console::pushCoTree(vector<RBT<Complex>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	double re, im;
	cin >> re >> im;
	v[index].push(Complex(re, im));
}
void Console::pushPeTree(vector<RBT<Person>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	cout << "Type first name, second name, last name, ID, birth date expression (must be integer)\n";
	cout << "Example: \"Joly-Maria Statsberg von-Grotte 10049 943330\"\n";
	string firstName, secondName, lastName;
	long long schoolID, birthDate;
	cin >> firstName >> secondName >> lastName >> schoolID >> birthDate;
	Person val(schoolID, firstName, secondName, lastName, birthDate);
	v[index].push(val);
}


void Console::findANode_int(vector<RBT<int>> v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	int val;
	cin >> val;
	string way;
	v[index].find(val, way);
	cout << "The way is\n" << way << endl;
}
void Console::findANode_dou(vector<RBT<double>> v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	double val;
	cin >> val;
	string way;
	v[index].find(val, way);
	cout << "The way is\n" << way << endl;
}
void Console::findANode_com(vector<RBT<Complex>> v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value (real then imaginary)\n";
	cout << "........................\n";
	double re, im;
	cin >> re >> im;
	Complex val(re, im);
	string way;
	v[index].find(val, way);
	cout << "The way is\n" << way << endl;
}
void Console::findANode_per(vector<RBT<Person>> v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	cout << "Type first name, second name, last name, ID, birth date expression (must be integer)\n";
	cout << "Example: \"Joly-Maria Statsberg von-Grotte 10049 943330\"\n";
	string firstName, secondName, lastName;
	long long schoolID, birthDate;
	cin >> firstName >> secondName >> lastName >> schoolID >> birthDate;
	Person val(schoolID, firstName, secondName, lastName, birthDate);
	string way;
	v[index].find(val, way);
	cout << "The way is\n" << way << endl;
}


void Console::rmInTree(vector<RBT<int>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	int val;
	cin >> val;
	v[index].remove(val);
}
void Console::rmDoTree(vector<RBT<double>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	double val;
	cin >> val;
	v[index].remove(val);
}
void Console::rmCoTree(vector<RBT<Complex>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	double re, im;
	cin >> re >> im;
	v[index].remove(Complex(re, im));
}
void Console::rmPeTree(vector<RBT<Person>>& v, size_t index) {
	if (index >= v.size()) {
		tryAgain();
		return;
	}
	cout << "........................\n";
	cout << "Type the value\n";
	cout << "........................\n";
	cout << "Type first name, second name, last name, ID, birth date expression (must be integer)\n";
	cout << "Example: \"Joly-Maria Statsberg von-Grotte 10049 943330\"\n";
	string firstName, secondName, lastName;
	long long schoolID, birthDate;
	cin >> firstName >> secondName >> lastName >> schoolID >> birthDate;
	Person val(schoolID, firstName, secondName, lastName, birthDate);
	v[index].remove(val);
}


void Console::crtATree() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to create?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	int choice;
	cin >> choice;
	if (choice > 4 || choice < 1) {
		Console::tryAgain();
		return;
	}
	size_t n;
	cout << "How many nodes do you want me create in the tree?\n";
	cin >> n;
	switch (choice)
	{
	case 1: {
		Console::crtIntTree(this->vI, n);
		break;
	}
	case 2: {
		Console::crtDouTree(this->vD, n);
		break;
	}
	case 3: {
		Console::crtComTree(this->vC, n);
		break;
	}
	case 4: {
		Console::crtPerTree(this->vP, n);
		break;
	}
	}
}
void Console::pushInTree() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to push a new node to?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	int choice;
	cin >> choice;
	if (choice < 1 || choice > 4)
	{
		Console::tryAgain();
		return;
	}
	size_t index;
	cout << "Type the tree's index (starting from 0)\n";
	cin >> index;
	switch (choice)
	{
	case 1: {
		Console::pushInTree(this->vI, index);
		break;
	}
	case 2: {
		Console::pushDoTree(this->vD, index);
		break;
	}
	case 3: {
		Console::pushCoTree(this->vC, index);
		break;
	}
	case 4: {
		Console::pushPeTree(this->vP, index);
		break;
	}
	default:
		break;
	}
}
void Console::findNode() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to find the path to a node?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	int choice;
	cin >> choice;
	if (choice < 1 || choice > 4)
	{
		Console::tryAgain();
		return;
	}
	size_t index;
	cout << "Type the tree's index (starting from 0)\n";
	cin >> index;
	switch (choice)
	{
	case 1: {
		Console::findANode_int(this->vI, index);
		break;
	}
	case 2: {
		Console::findANode_dou(this->vD, index);
		break;
	}
	case 3: {
		Console::findANode_com(this->vC, index);
		break;
	}
	case 4: {
		Console::findANode_per(this->vP, index);
		break;
	}
	default:
		break;
	}
	cout << "That is path down from the root. \'r\' represents the right child, \'l\' represents the left child\n";
}
void Console::rmNode() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to remove a node from?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	int choice;
	cin >> choice;
	if (choice < 1 || choice > 4)
	{
		Console::tryAgain();
		return;
	}
	size_t index;
	cout << "Type the tree's index (starting from 0)\n";
	cin >> index;
	switch (choice)
	{
	case 1: {
		Console::rmInTree(this->vI, index);
		break;
	}
	case 2: {
		Console::rmDoTree(this->vD, index);
		break;
	}
	case 3: {
		Console::rmCoTree(this->vC, index);
		break;
	}
	case 4: {
		Console::rmPeTree(this->vP, index);
		break;
	}
	default:
		break;
	}
}
void Console::seeIfEqual() {
	cout << "------------------------\n";
	cout << "What kind of trees do you want me to compare?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	int choice;
	cin >> choice;
	if (choice > 4 || choice < 1) {
		Console::tryAgain();
		return;
	}
	size_t index1, index2;
	cout << "Type the indexes (start from 0)\n";
	cin >> index1 >> index2;
	if (index1 == index2) {
		cout << "If they exist, they are equal\n";
		return;
	}
	bool are = 1;
	switch (choice)
	{
	case 1:
	{
		if (index1 >= vI.size() || index2 >= vI.size())
		{
			tryAgain();
			return;
		}
		are = (vI[index1] == vI[index2]);
		break;
	}
	case 2:
	{
		if (index1 >= vD.size() || index2 >= vD.size())
		{
			tryAgain();
			return;
		}
		are = (vD[index1] == vD[index2]);
		break;
	}
	case 3:
	{
		if (index1 >= vC.size() || index2 >= vC.size())
		{
			tryAgain();
			return;
		}
		are = (vC[index1] == vC[index2]);
		break;
	}
	case 4:
	{
		if (index1 >= vP.size() || index2 >= vP.size())
		{
			tryAgain();
			return;
		}
		are = (vP[index1] == vP[index2]);
		break;
	}
	}
	cout << ((are) ? "Yep, exactly the same\n" : "Nope, completely different\n");
}
void Console::howManyTrees() {
	cout << "------------------------\n";
	cout << "Integer trees: " << vI.size() << endl;
	cout << "Double trees: " << vD.size() << endl;
	cout << "Complex trees: " << vC.size() << endl;
	cout << "Person trees: " << vP.size() << endl;
	cout << "------------------------\n";
}
void Console::getAsString() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to get as string?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	size_t choice, index;
	cin >> choice;
	if (choice < 1 || choice > 4)
	{
		tryAgain();
		return;
	}
	cout << "Type 3 chars: exactly one \'0\', exactly one \'1\' and exactly one\'2\'\nThis will define traversal, for example \"102\" defines centralized traversal and \"012\" defines left-oriented traversal\n";
	string s;
	cin >> s;
	if (redBlackTree<int>::incorrectTraversal(s))
	{
		tryAgain();
		return;
	}
	cout << "Type index of the RB-tree\n";
	cin >> index;
	switch (choice)
	{
	case 1:
	{
		if (index >= vI.size())
		{
			tryAgain();
			return;
		}
		cout << vI[index].asString(s);
		break;
	}
	case 2:
	{
		if (index >= vD.size())
		{
			tryAgain();
			return;
		}
		cout << vD[index].asString(s);
		break;
	}
	case 3:
	{
		if (index >= vC.size())
		{
			tryAgain();
			return;
		}
		cout << vC[index].asString(s);
		break;
	}
	case 4:
	{
		if (index >= vP.size())
		{
			tryAgain();
			return;
		}
		cout << vP[index].asString(s);
		break;
	}
	default:
		break;
	}
}
void Console::dltTree() {
	cout << "------------------------\n";
	cout << "What kind of tree do you want me to delete?\n";
	cout << "1 -- int\n";
	cout << "2 -- double\n";
	cout << "3 -- Complex\n";
	cout << "4 -- Person\n";
	cout << "------------------------\n";
	size_t choice, index;
	cin >> choice;
	if (choice < 1 || choice > 4)
	{
		tryAgain();
		return;
	}
	cout << "Type index of the RB-tree\n";
	cin >> index;
	switch (choice)
	{
	case 1:
	{
		if (index >= vI.size())
		{
			tryAgain();
			return;
		}
		vI.erase(vI.begin() + index);
		break;
	}
	case 2:
	{
		if (index >= vD.size())
		{
			tryAgain();
			return;
		}
		vD.erase(vD.begin() + index);
		break;
	}
	case 3:
	{
		if (index >= vC.size())
		{
			tryAgain();
			return;
		}
		vC.erase(vC.begin() + index);
		break;
	}
	case 4:
	{
		if (index >= vP.size())
		{
			tryAgain();
			return;
		}
		vP.erase(vP.begin() + index);
		break;
	}
	}

}
void Console::standardAbracadabra() {
	cout << "------------------------\n";
	cout << "If you want to try traversals,then\n";
	cout << "please, do it manually. You have to\n";
	cout << "create a redBlackTree<type> object,\n";
	cout << "fill it, using method \'push\', and\n";
	cout << "either use method traversalMap, which\n";
	cout << "does whatever you feed into the method\n";
	cout << "to copies of every node in the tree or\n";
	cout << "you can use static method in another\n";
	cout << "class: Traversal::traversalReduce. It\n";
	cout << "does whatever you feed the method to\n";
	cout << "copies of every node, but it can also\n";
	cout << "collect data while being executed. For\n";
	cout << "example, following command, applyed to\n";
	cout << "a redBlackTree<int> object theTree calculates\n";
	cout << "sum of every integer in the nodes in the tree\n"; 
	cout << "and puts the result into the preinitialized\n";
	cout << "with zero int sum:\n";
	cout << "Traversal::traversalReduce<int, int>(\"102\", [](int& start, RB<int>* nd) {start += *(nd->getInfo()); }, sum, theTree);\n";
	cout << "You can also run threaded traversal with\n";
	cout << "threadedTraversalMap method\n";
	cout << "------------------------\nDid you manage to read this? Type something" << endl;
	string s;
	getline(cin, s);
	getline(cin, s);
}

void Console::mainMenu() {
	cout << "########################\n";
	cout << "What do you want me to do?\n";
	cout << "0 -- Quit\n";
	cout << "1 -- Create an RB-tree\n";
	cout << "2 -- Push new node into an exsisting RB-tree\n";
	cout << "3 -- Find a node's path from the root\n";
	cout << "4 -- Remove a node from an existin RB-tree\n";
	cout << "5 -- See if two trees are equal (e.g. contain same info in same order)\n";
	cout << "6 -- See how many trees you've created\n";
	cout << "7 -- Look at a tree as a string\n";
	cout << "8 -- Delete an RB-tree\n";
	cout << "9 -- Do a (threaded) traversal on a tree\n";
	cout << "#######################\n";
	int choice;
	cin >> choice;
	switch (choice)
	{
	case 0: {
		exit(0);
	}
	case 1:
	{
		crtATree();
		break;
	}
	case 2:
	{
		pushInTree();
		break;
	}
	case 3:
	{
		findNode();
		break;
	}
	case 4:
	{
		rmNode();
		break;
	}
	case 5:
	{
		seeIfEqual();
		break;
	}
	case 6:
	{
		howManyTrees();
		break;
	}
	case 7:
	{
		getAsString();
		break;
	}
	case 8:
	{
		dltTree();
		break;
	}
	case 9:
	{
		standardAbracadabra();
		break;
	}
	default:
		Console::tryAgain();
		break;
	}
}

void Console::console()
{
	while (true)
		mainMenu();
}