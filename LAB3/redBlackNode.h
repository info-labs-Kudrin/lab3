#pragma once
#include "Complex.h"
#include "HashGenerator.h"
#include "School.h"

#define RB redBlackNode

const int ERROR_INCORRECT_COLOR = 1;

template <class T>
class redBlackNode
{
private:
	int color; // 0 -- red, 1 -- black (red-black), 2 -- double black
	T* info;
	long long key;
public:
	RB<T>* left, * right, * parent, * additional;

	redBlackNode();
	redBlackNode(T val);
	redBlackNode(T val, RB<T>* EList);
	redBlackNode(T val, RB<T>* EList, RB<T>* Parent);
	~redBlackNode();

	void prt();
	int getColor();
	void setColor(int a);
	long long getKey();
	T* getInfo();

	bool operator==(const RB<T>& other);
};

