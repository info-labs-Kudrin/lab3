#pragma once
#include <cmath>
#include "Complex.h"
#include "School.h"

long long getHash(int value);

long long getHash(double value);

long long getHash(Complex value);

long long getHash(Person value);