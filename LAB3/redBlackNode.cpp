#include "redBlackNode.h"

template redBlackNode<int>;
template redBlackNode<double>;
template redBlackNode<Complex>;
template redBlackNode<Person>;

template <class T>
redBlackNode<T>::redBlackNode() {
	color = 0;
	info = new T(0);
	key = getHash(*info);
	left = 0;
	right = 0;
	parent = 0;
	additional = 0;
}

template<class T>
redBlackNode<T>::redBlackNode(T val)
{
	color = 0;
	info = new T(val);
	key = getHash(val);
	left = 0;
	right = 0;
	parent = 0;
	additional = 0;
}

template<class T>
redBlackNode<T>::redBlackNode(T val, RB<T>* EList) : redBlackNode(val, EList, 0) { }

template<class T>
redBlackNode<T>::redBlackNode(T val, RB<T>* EList, RB<T>* Parent) : redBlackNode(val)
{
	left = EList;
	right = EList;
	this->parent = Parent;
}

template<class T>
redBlackNode<T>::~redBlackNode()
{
	delete info;
}


template <class T>
int redBlackNode<T>::getColor() {
	return this->color;
}

template <class T>
void redBlackNode<T>::setColor(int a) {
	if (a < 0 || a > 2)
		throw ERROR_INCORRECT_COLOR;
	this->color = a;
}

template<class T>
long long redBlackNode<T>::getKey()
{
	return this->key;
}

template<class T>
T* redBlackNode<T>::getInfo() {
	return info;
}

/// <summary>
/// Same info
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name=""></param>
/// <returns></returns>
template <class T>
bool redBlackNode<T>::operator==(const RB<T>& other)
{
	if (this->color != other.color)
		return 0;
	if (this->key != other.key)
		return 0;
	return 1;
}

template <>
void redBlackNode<int>::prt()
{
	printf("Hash: %5lld\tvalue: %12d\tcolor: %5s\n", this->key, *(this->info), (this->color == 0) ? "RED" : "BLACK");
}
template <>
void redBlackNode<double>::prt()
{
	printf("Hash: %5lld\tvalue: %12lf\tcolor: %5s\n", this->key, *(this->info), (this->color == 0) ? "RED" : "BLACK");
}
template <>
void redBlackNode<Complex>::prt()
{
	printf("Hash: %5lld\tvalue: %5lf+%5lfi\tcolor: %5s\n", this->key, this->info->getReal(), this->info->getImag(), (this->color == 0) ? "RED" : "BLACK");
}
template <>
void redBlackNode<Person>::prt()
{
	this->getInfo()->prt();
}