#include "redBlackTree.h"

template redBlackTree<int>;
template redBlackTree<double>;
template redBlackTree<Complex>;
template redBlackTree<Person>;

template <class T>
redBlackTree<T>::redBlackTree(const redBlackTree<T>& other) : redBlackTree()
{
	if (other.root == 0)
		return;
	root = new RB<T>(*(other.root->getInfo()));
	root->setColor(1);	
	if (other.root->left != other.EList)
	{
		root->left = getSubRecurrent(other.root->left, other.EList, EList, root);
	}
	else
		root->left = EList;
	if (other.root->right != other.EList)
	{
		root->right = getSubRecurrent(other.root->right, other.EList, EList, root);
	}
	else
		root->right = EList;
}

template <class T>
RB<T>* redBlackTree<T>::findAdress(const T& val, RB<T>* nd, RB<T>* EList)
{
	RB<T>* cur = nd;
	long long key = getHash(val);
	while (cur != EList && cur->getKey() != key)
	{
		if (cur->getKey() < key)
			cur = cur->right;
		else
			cur = cur->left;
	}
	if (cur == EList)
		throw ERROR_INCORRECT_NODE;
	return cur;
}

template<class T>
inline bool redBlackTree<T>::incorrectTraversal(string how)
{
	if (how.size() != 3)
		return true;
	if (how[0] != '0' && how[1] != '0' && how[2] != '0')
		return true;
	if (how[0] != '1' && how[1] != '1' && how[2] != '1')
		return true;
	if (how[0] != '2' && how[1] != '2' && how[2] != '2')
		return true;
	return false;
}

template<class T>
void redBlackTree<T>::turnLeft(RB<T>* turnedNode)
{
	RB<T>* rl = turnedNode->right->left, *r = turnedNode->right, *par = turnedNode->parent;
	if (r == EList)
		throw ERROR_INCORRECT_NODE;

	turnedNode->right = rl;
	turnedNode->parent = r;
	
	if (par->left == turnedNode)
		par->left = r;
	else
		par->right = r;

	r->parent = par;
	r->left = turnedNode;

	rl->parent = turnedNode;

	root = EList->left;
}

template<class T>
void redBlackTree<T>::turnRight(RB<T>* turnedNode)
{
	RB<T>* lr = turnedNode->left->right, * l = turnedNode->left, *par = turnedNode->parent;
	if (l == EList)
		throw ERROR_INCORRECT_NODE;

	turnedNode->left = lr;
	turnedNode->parent = l;

	if (par->left == turnedNode)
		par->left = l;
	else
		par->right = l;

	l->parent = par;
	l->right = turnedNode;

	lr->parent = turnedNode;

	root = EList->left;
}

template<class T>
redBlackTree<T>::redBlackTree()
{
	root = 0;
	EList = new RB<T>();
	EList->setColor(1);
}

template<class T>
void redBlackTree<T>::push(const T &info)
{
	if (root == 0)
	{
		root = new RB<T>(info, EList, EList);
		root->setColor(1);
		EList->left = root;
		return;
	}
	long long newKey = getHash(info);
	RB<T>* cur = root,* prev = cur;
	while (cur != EList)
	{
		prev = cur;
		cur = ((cur->getKey() >= newKey) ? cur->left : cur->right);
	}
	cur = prev;
	if (cur->getKey() >= newKey)
	{
		cur->left = new RB<T>(info, EList, cur);
		cur = cur->left;
	}
	else
	{
		cur->right = new RB<T>(info, EList, cur);
		cur = cur->right;
	}
	correctPush(cur);
}

template<class T>
void redBlackTree<T>::correctPush(RB<T>* nd)
{
	while (true) {
		RB<T>* par = nd->parent, *parpar = par->parent;
		if (par->getColor() == 1)
			return;
		if (parpar->left->getColor() == 0 && parpar->right->getColor() == 0)
		{
			parpar->left->setColor(1);
			parpar->right->setColor(1);
			parpar->setColor(0);
			if (parpar == root) {
				root->setColor(1);
				return;
			}
			nd = parpar;
		}
		else if (isLeftChild(nd, EList) && !isLeftChild(nd->parent, EList))
		{
			turnRight(nd->parent);
			nd = nd->right;
		}
		else if (!isLeftChild(nd, EList) && isLeftChild(nd->parent, EList))
		{
			turnLeft(nd->parent);
			nd = nd->left;
		}
		else if (isLeftChild(nd, EList)) {
			par->setColor(1);
			parpar->setColor(0);
			turnRight(parpar);
			return;
		}
		else {
			par->setColor(1);
			parpar->setColor(0);
			turnLeft(parpar);
			return;
		}
	}

}

template<class T>
bool redBlackTree<T>::isLeftChild(RB<T>* nd, RB<T>* EList)
{
	if (nd == EList)
		throw ERROR_INCORRECT_NODE;
	if (nd->parent->left == nd)
		return true;
	return false;
}

template<class T>
bool redBlackTree<T>::noNode(char c, RB<T>* nd, RB<T>* EList)
{
	switch (c)
	{
	case '0':
	{
		if (nd->left == EList)
			return 1;
		return 0;
		break;
	}
	case '1':
	{
		return 1;
		break;
	}
	case '2':
	{
		if (nd->right == EList)
		{
			return 1;
		}
		return 0;
		break;
	}
	default:
		throw ERROR_INCORRECT_TRAVERSAL;
		break;
	}
	return false;
}

template<class T>
void redBlackTree<T>::traversalRecurrent(string how, function<void(RB<T>*)> f, RB<T>* nd, RB<T>* EList)
{
	for (size_t i = 0; i < how.size(); i++)
	{
		switch (how[i])
		{
		case '0':
		{
			if (nd->left != EList)
				traversalRecurrent(how, f, nd->left, EList);
			break;
		}
		case '1':
		{
			RB<T>* copy = new RB<T>(*(nd->getInfo()));
			copy->setColor(nd->getColor());
			f(copy);
			delete copy;
			break;
		}
		case '2':
		{
			if (nd->right != EList)
				traversalRecurrent(how, f, nd->right, EList);
			break;
		}
		}
	}
}

template<class T>
void redBlackTree<T>::find(RB<T>* val, string& way, RB<T>* cur, RB<T>* EList)
{
	//cur is start point
	long long lookingFor = val->getKey();
	way = "";
	while (cur != EList && (cur->getKey() != lookingFor || cur != val))
	{
		if (lookingFor <= cur->getKey()) {
			cur = cur->left;
			way.push_back('l');
		}
		else {
			cur = cur->right;
			way.push_back('r');
		}
	}
	if (cur != EList)
		return;
	way = "";
	throw ERROR_UNKNOWN;
}

template<class T>
bool redBlackTree<T>::find(const T &val, string& way)
{
	if (root == 0)
		return 0;
	long long lookingFor = getHash(val);
	RB<T>* cur = root;
	way = "";
	while (cur != EList && cur->getKey() != lookingFor)
	{
		if (lookingFor <= cur->getKey()) {
			cur = cur->left;
			way.push_back('l');
		}
		else {
			cur = cur->right;
			way.push_back('r');
		}
	}
	if (cur != EList)
		return 1;
	way = "";
	return 0;
}

/// <summary>
/// Applies f to every node, no connectrion among applications
/// Do NOT delete your node in f
/// 0 -- left, 1 -- center, 2 -- right
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="how"></param>
/// <param name="f"></param>
template<class T>
void redBlackTree<T>::traversalMap(string how, function<void(RB<T>*)> f)
{
	if (incorrectTraversal(how))
		throw ERROR_INCORRECT_TRAVERSAL;
	if (root == 0)
		return;
	traversalRecurrent(how, f, root, EList);
}

template<class T>
RB<T>* redBlackTree<T>::getNodeCopy(const T &val)
{
	RB<T>* cur = this->findAdress(val, root, this->EList);
	RB<T>* res = new RB<T>(*(cur->getInfo()));
	res->setColor(cur->getColor());
	return res;
}

template<class T>
void redBlackTree<T>::threading(string how)
{
	queue<RB<T>*> qu;
	Traversal::traversalReduce_noCopies<queue<RB<T>*>, T>(how, [](queue<RB<T>*> &q, RB<T>* cur) {q.push(cur); }, qu, *this);

	RB<T>* cur = qu.front(); qu.pop();
	while (!qu.empty())
	{
		cur->additional = qu.front();
		cur = cur->additional;
		qu.pop();
	}
	cur->additional = EList;
	EList->additional = root;
}

//Do NOT delete your node in f
template<class T>
void redBlackTree<T>::threadedTraversalMap(string how, void (*f)(RB<T>*))
{
	this->threading(how);
	RB<T>* cur = root, * prev = cur;
	if (how[0] != '1')
	{
		while (cur != EList)
		{
			if (how[0] == '0')
			{
				prev = cur;
				cur = cur->left;
			}
			else
			{
				prev = cur;
				cur = cur->right;
			}
		}
	}
	cur = prev;
	while (cur != EList)
	{
		RB<T>* x = new RB<T>(*(cur->getInfo()));
		x->setColor(cur->getColor());
		f(x);
		delete x;
		cur = cur->additional;
	}
}

template <class T>
bool redBlackTree<T>::areEqualRecurrent(RB<T>* cur1, RB<T>* cur2, RB<T>* EList1, RB<T>* EList2)
{
	if (!(*cur1 == *cur2) || ((cur1->left == EList1) != (cur2->left == EList2)) || ((cur1->right == EList1) != (cur2->right == EList2)))
		return 0;
	bool res = 1;
	if (cur1->left != EList1)
		res = res * (areEqualRecurrent(cur1->left, cur2->left, EList1, EList2));
	if (cur1->right != EList1)
		res = res * (areEqualRecurrent(cur1->right, cur2->right, EList1, EList2));
	return res;
}

template<class T>
bool redBlackTree<T>::operator==(const redBlackTree <T>& other)
{
	RB<T>* cur1 = this->root, * cur2 = other.root;
	if (cur1 == 0)
	{
		if (cur2 != 0)
			return 0;
		return 1;
	}
	if (cur2 == 0)
	{
		if (cur1 != 0)
			return 0;
		return 1;
	}
	return (areEqualRecurrent(cur1, cur2, this->EList, other.EList));
}

template<class T>
RB<T>* redBlackTree<T>::getSubRecurrent(RB<T>* from, RB<T>* EList_from, RB<T>* EList_to, RB<T>* parent)
{
	RB<T>* to = new RB<T>(*(from->getInfo()));
	to->setColor(from->getColor());
	to->parent = parent;
	if (from->left != EList_from)
		to->left = getSubRecurrent(from->left, EList_from, EList_to, to);
	else
		to->left = EList_to;
	if (from->right != EList_from)
		to->right = getSubRecurrent(from->right, EList_from, EList_to, to);
	else
		to->right = EList_to;
	return to;
}

template <class T>
redBlackTree<T>* redBlackTree<T>::getSubTree(const T &from)
{
	RB<T>* cur = this->findAdress(from, this->root, this->EList);
	redBlackTree<T>* res = new redBlackTree<T>();
	res->root = getSubRecurrent(cur, this->EList, res->EList, res->EList);
	res->EList->left = res->root;
	return res;
}

template <class T>
void redBlackTree<T>::remove(const T &val)
{
	redBlackTree<T> *res = Traversal::RB_Where<T>([&val](RB<T>* nd)->bool {return (*(nd->getInfo()) != val ); }, this);
	this->~redBlackTree();
	this->root = res->root;
	this->EList = res->EList;
}

template <class T>
void redBlackTree<T>::decomposeRecurrent(RB<T>* nd)
{
	if (nd->left != this->EList)
		decomposeRecurrent(nd->left);
	if (nd->right != this->EList)
		decomposeRecurrent(nd->right);
	delete nd;
}

template <class T>
redBlackTree<T>::~redBlackTree()
{
	if (this->root != 0)
		decomposeRecurrent(this->root);
	delete this->EList;
}

template <class T>
bool redBlackTree<T>::findSubTree(const redBlackTree<T>& other, string& way)
{
	way = "";
	bool found = 0;
	RB<T>* forFound = 0,* EL = EList;
	this->traversalMap_noCopies("102", [&found, &other, &EL, &forFound](RB<T>* nd)
		{
			if (redBlackTree<T>::areEqualRecurrent(nd, other.root, EL, other.EList))
			{
				found = 1;
				forFound = nd;
			}
		});
	if (!found)
		return 0;
	find(forFound, way, this->root, this->EList);
	return 1;
}


template<class T>
void redBlackTree<T>::traversalMap_noCopies(string how, function<void(RB<T>*)> f)
{
	if (incorrectTraversal(how))
		throw ERROR_INCORRECT_TRAVERSAL;
	if (root == 0)
		return;
	traversalRecurrent_noCopies(how, f, root, EList);
}

template<class T>
void redBlackTree<T>::traversalRecurrent_noCopies(string how, function<void(RB<T>*)> f, RB<T>* nd, RB<T>* EList)
{
	for (size_t i = 0; i < how.size(); i++)
	{
		switch (how[i])
		{
		case '0':
		{
			if (nd->left != EList)
				traversalRecurrent_noCopies(how, f, nd->left, EList);
			break;
		}
		case '1':
		{
			f(nd);
			break;
		}
		case '2':
		{
			if (nd->right != EList)
				traversalRecurrent_noCopies(how, f, nd->right, EList);
			break;
		}
		}
	}
}

template <class T>
vector<T> redBlackTree<T>::asVector(string how)
{
	vector<T> res(0);
	Traversal::traversalReduce_noCopies<vector<T>, T>(how, [](vector<T>& res, RB<T>* nd)
		{
			res.push_back(*(nd->getInfo()));
		}, res, *this);
	return res;
}

template <>
string redBlackTree<int>::asString(string how)
{
	vector<int> r = this->asVector(how);
	string res = "";
	for (int el : r)
	{
		res = res + std::to_string(el) + ' ';
	}
	return res;
}
template <>
string redBlackTree<double>::asString(string how)
{
	vector<double> r = this->asVector(how);
	string res = "";
	for (double el : r)
	{
		res = res + std::to_string(el) + ' ';
	}
	return res;
}
template <>
string redBlackTree<Complex>::asString(string how)
{
	vector<Complex> r = this->asVector(how);
	string res = "";
	for (Complex el : r)
	{
		res = res + std::to_string(el.getReal()) + ' ' +  std::to_string(el.getImag()) + "i ";
	}
	return res;
}

template <>
string redBlackTree<Person>::asString(string how)
{
	vector<Person> r = this->asVector(how);
	string res = "";
	for (Person el : r)
	{
		res = res + std::to_string(el.getID()) + ". " + el.getFirstName() + ' ' + el.getMiddleName() + ' ' + el.getLastName() +", birthvalue: " + std::to_string(el.getBirthDate()) + "\n";
	}
	return res;
}

template <class T>
redBlackTree<T>::redBlackTree(T* arr, size_t size) : redBlackTree<T>()
{
	for (size_t i = 0; i < size; i++)
		this->push(arr[i]);
}
