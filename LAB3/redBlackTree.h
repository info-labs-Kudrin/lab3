#pragma once
#include <string>
#include <queue>
#include <vector>
#include <functional>
#include "redBlackNode.h"
#include "Complex.h"
#include "HashGenerator.h"
#include "School.h"

const int ERROR_INCORRECT_NODE = 1;
const int ERROR_INCORRECT_TRAVERSAL = 2;
const int ERROR_UNKNOWN = 3;

template <class T>
class redBlackTree 
{
	friend class Traversal;
private:
	RB<T>* root;
	RB<T>* EList;
	void turnLeft(RB<T> *turnedNode);
	void turnRight(RB<T> *turnedNode);
	void correctPush(RB<T>* nd);

	void traversalMap_noCopies(string how, function<void(RB<T>*)> f);
	static void traversalRecurrent_noCopies(string how, function<void(RB<T>*)> f, RB<T>* nd, RB<T>* EList);

	static void find(RB<T>* val, string& way, RB<T>* cur, RB<T>* EList);
	static RB<T>* findAdress(const T &val, RB<T>* nd, RB<T>* EList);
	static bool isLeftChild(RB<T>* nd, RB<T>* EList);
	static bool noNode(char c, RB<T>* nd, RB<T>* EList);
	static void traversalRecurrent(string how, function<void(RB<T>*)> f, RB<T>* nd, RB<T>* EList);
	static RB<T>* getSubRecurrent(RB<T>* from, RB<T>* EList_from, RB<T>* EList_to, RB<T>* parent);
	static bool areEqualRecurrent(RB<T>* cur1, RB<T>* cur2, RB<T>* EList1, RB<T>* EList2);

public:
	redBlackTree();
	redBlackTree(T* arr, size_t size);
	redBlackTree(const redBlackTree<T>& other);

	static bool incorrectTraversal(string how);
	void push(const T &info);
	bool find(const T &val, string& way);
	void traversalMap(string how, function<void(RB<T>*)> f);
	RB<T>* getNodeCopy(const T &val);
	void threadedTraversalMap(string how, void (*f)(RB<T>*));
	void threading(string how);
	void remove(const T &val);

	bool operator==(const redBlackTree <T>& other);
	redBlackTree<T>* getSubTree(const T &from);
	bool findSubTree(const redBlackTree<T>& other, string& way);

	vector<T> asVector(string how);
	string asString(string how);
	void decomposeRecurrent(RB<T>* nd);
	~redBlackTree();
};

class Traversal
{
	friend class redBlackTree<int>;
	friend class redBlackTree<double>;
	friend class redBlackTree<Complex>;
	friend class redBlackTree<Person>;

private:
	template <class T1, class T2>
	static void traversalRecurrentReduce(string how, void (*f)(T1&, RB<T2>*), T1 &val, RB<T2>* nd, RB<T2>* EList)
	{
		for (size_t i = 0; i < how.size(); i++)
		{
			switch (how[i])
			{
			case '0':
			{
				if (nd->left != EList)
					traversalRecurrentReduce(how, f, val, nd->left, EList);
				break;
			}
			case '1':
			{
				RB<T2>* copy = new RB<T2>(*(nd->getInfo()));
				copy->setColor(nd->getColor());
				f(val, copy);
				delete copy;
				break;
			}
			case '2':
			{
				if (nd->right != EList)
					traversalRecurrentReduce(how, f, val, nd->right, EList);
				break;
			}
			}
		}
	}
	
	template <class T1, class T2>
	static void traversalRecurrentReduce_noCopies(string how, function<void(T1&, RB<T2>*)> f, T1& val, RB<T2>* nd, RB<T2>* EList)
	{
		for (size_t i = 0; i < how.size(); i++)
		{
			switch (how[i])
			{
			case '0':
			{
				if (nd->left != EList)
					traversalRecurrentReduce_noCopies(how, f, val, nd->left, EList);
				break;
			}
			case '1':
			{
				f(val, nd);
				break;
			}
			case '2':
			{
				if (nd->right != EList)
					traversalRecurrentReduce_noCopies(how, f, val, nd->right, EList);
				break;
			}
			}
		}
	}

	template <class T1, class T2>
	static void traversalReduce_noCopies(string how, function<void(T1&, RB<T2>*)> f, T1& val, redBlackTree<T2>& tree)
	{
		if (redBlackTree<T2>::incorrectTraversal(how))
			throw ERROR_INCORRECT_TRAVERSAL;
		if (tree.root == 0)
			return;
		Traversal::traversalRecurrentReduce_noCopies(how, f, val, tree.root, tree.EList);
	}
public:
	template <class T1, class T2>
	static void traversalReduce(string how, void (*f)(T1&, RB<T2>*), T1 &val, redBlackTree<T2>& tree)
	{
		if (redBlackTree<T2>::incorrectTraversal(how))
			throw ERROR_INCORRECT_TRAVERSAL;
		if (tree.root == 0)
			return;
		Traversal::traversalRecurrentReduce(how, f, val, tree.root, tree.EList);
	}

	template <class T> 
	static redBlackTree<T> *RB_Where(function<bool(RB<T>*)> predicate, redBlackTree<T>* tree)
	{
		redBlackTree<T>* res = new redBlackTree<T>();
		traversalReduce_noCopies<redBlackTree<T>*, T>("102", [predicate](auto res, RB<T>* nd) {
				if (predicate(nd))
					res->push(*(nd->getInfo()));
			}, res, *tree);
		return res;
	}
};